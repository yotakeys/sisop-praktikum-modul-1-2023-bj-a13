#!/bin/bash

<<soal
Bocchi hendak melakukan University Admission Test di Jepang. 
Bocchi ingin masuk ke universitas yang bagus. 
Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. 
Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi
soal

<<a
Bocchi ingin masuk ke universitas yang bagus di Jepang. 
Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. 
Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
a

top_5_rank=$(awk -F , '$3 ~ /JP/' '/home/key/sisop-praktikum-modul-1-2023-bj-a13/soal1/data.csv' | sort -t , -nk1 | head -n 5)
top_5_rank_university=$(echo "$top_5_rank" | awk -F ',' '{print $2 ","}')

echo "Top 5 University int Japan : "
echo $top_5_rank_university
echo " "

<<b
Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) 
yang paling rendah diantara 5 Universitas dari hasil filter poin a.
b

# fsrlowest=$(echo "$top_5_rank" | sort -t , -nk9 | head -n 1 | awk -F , '{print $2 }')

# echo "Lowest FSR Score from Top 5 Rank University in Japan :"
# echo $fsrlowest
# echo " "

<<bnew
baru :
Karena Bocchi kurang percaya diri dengan kemampuannya, 
coba cari Faculty Student Score(fsr score) yang paling 
rendah diantara 5 Universitas di Jepang. 
bnew

fsrlowest=$(awk -F , '$3 ~ /JP/' '/home/key/sisop-praktikum-modul-1-2023-bj-a13/soal1/data.csv' | sort -t , -nk9 | head -n 5 | awk -F , '{print $2 ","}')

echo "Lowest FSR Score University in Japan :"
echo $fsrlowest
echo " "

<<c
Karena Bocchi takut salah membaca ketika memasukkan nama universitas, 
cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
c

top_10_ger_rank=$(awk -F , '$3 ~ /JP/' '/home/key/sisop-praktikum-modul-1-2023-bj-a13/soal1/data.csv' | sort -t , -nk20 | head -n 10 | awk -F , '{print $2 ","}')

echo "Top 10 Ger Rank Tertinggi : "
echo $top_10_ger_rank
echo " "

<<d
Bocchi ngefans berat dengan universitas paling keren di dunia. 
Bantu bocchi mencari universitas tersebut dengan kata kunci keren.
d

keren_university=$(awk '/[Kk]eren/' '/home/key/sisop-praktikum-modul-1-2023-bj-a13/soal1/data.csv' | awk -F , '{ print $2 }')

echo "University ter Keren : "
echo $keren_university