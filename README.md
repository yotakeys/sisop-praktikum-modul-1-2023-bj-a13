# Sisop Praktikum Modul 1 2023 BJ-A13

Anggota : 
1. Keyisa Raihan I. S.  (5025211002)
2. M. Taslam Gustino    (5025211011)
3. Adrian Karuna S.     (5025211019)
4. Vito Febrian Ananta  (5025211224)

## Soal 1

Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi

##### Soal a

Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.

Penyelesaian : 
```
top_5_rank=$(awk -F , '$3 ~ /JP/' '/home/key/sisop-praktikum-modul-1-2023-bj-a13/soal1/data.csv' | sort -t , -nk1 | head -n 5)
top_5_rank_university=$(echo "$top_5_rank" | awk -F ',' '{print $2 ","}')

echo "Top 5 University int Japan : "
echo $top_5_rank_university
echo " "
```

Penjelasan : 
- Menggunakan AWK kita membaca file data CSV, dengan menggunakan separator “,” kita ambil semua row yang kolom ke-3 nya berisi “JP untuk mengambil semua universitas yang berada di jepang
- Hasil tersebut kita sort berdasarkan kolom “Rank” atau kolom ke 1
- Lalu kita ambil 5 teratas menggunakan perintah head, hasilnya kita simpan pada variabel $top_5_rank
- Dari variabel $top_5_rank kita ambil datanya dan kita ambil kolom nama universitasnya saja pada kolom 2 menggunakan AWK, Lalu disimpan pada $top_5_rank_university
- Tinggal menampilkan var hasilnya menggunakan echo 

##### Soal b

Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di jepang.

Penyelesaian :
```
fsrlowest=$(awk -F , '$3 ~ /JP/' '/home/key/sisop-praktikum-modul-1-2023-bj-a13/soal1/data.csv' | sort -t , -nk9 | head -n 5 | awk -F , '{print $2 ","}')

echo "Lowest FSR Score University in Japan :"
echo $fsrlowest
echo " "
```

Penjelasan : 
- Sama seperti soal A, kita ambil data CSV. dengan separator "," kita ambil semua row yang kolm ke-3 nya berisi JP. 
- lalu kita sort berdasarkan kolom fsr score yaitu kolom ke 9
- kita ambil top 5, yaitu dengan nilai fsr terendah menggunakan head
- hasilnya kita ambil kolom ke 2 atau nama universitasnya menggunakan AWK
- terakhir mencetak hasil var nya dengan echo

*Note : Ini merupakan penjelasan soal b terbaru, ntuk penyelesaian b lama, dapat dilihat di repo

###### Soal c

Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.

Penyelesaian : 
```
top_10_ger_rank=$(awk -F , '$3 ~ /JP/' '/home/key/sisop-praktikum-modul-1-2023-bj-a13/soal1/data.csv' | sort -t , -nk20 | head -n 10 | awk -F , '{print $2 ","}')

echo "Top 10 Ger Rank Tertinggi : "
echo $top_10_ger_rank
echo " "
```

Penjelasan : 
- Sama seperti soal A, pertama kita baca file CSV, dengan separator “,” kita pilih row yang memiliki kolom ke-3 berisi JP
- Hasilnya kita sort berdasarkan kolom ger rank atau kolom ke-20
- kita ambil 10 teratas dengan gerrarnk tertinggi menggunakan head
- lalu kita ambil nama univeritynya menggunakan AWK
tinggal dicetak menggunakan Echo

##### Soal d

Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

Penyelesaian : 
```
keren_university=$(awk '/[Kk]eren/' '/home/key/sisop-praktikum-modul-1-2023-bj-a13/soal1/data.csv' | awk -F , '{ print $2 }')

echo "University ter Keren : "
echo $keren_university
```

Penjelasan : 
- Kita baca soal CSV nya, lalu kita cara row yang mengandung kata keren menggunakan AWK
- Lalu kita ambil nama university nya - kolom ke-2 menggunakan AWK Juga
- Terakhir tinggal dicetak menggunakan echo

##### Output 
![image](/uploads/47bc89772998bd7e8546fc34687ebeb6/image.png)

## Soal 2

Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

##### Soal a

Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst)

Penyelesaian :
```bash
touch crontab_temp
echo "0 */10 * * * ~/Sisop/Praktikum1/kobeni_liburan.sh" >> crontab_temp
crontab crontab_temp
echo "@daily ~/zip.sh" >> crontab_temp
crontab crontab_temp
rm -rf crontab_temp
```
```bash
time=$(date +%k)
number_folder=1;#the variable that I want to be increment
next_nfolder=$[$number_folder+1]
sed -i "/#the variable that I want to be increment$/s/=.*#/=$next_nfolder;#/" ${0}

number_file=1;#the variable that I want to be incremented
mkdir ~/kumpulan_$number_folder

if [ $time == 0 ]
then
    wget -O ~/kumpulan_$number_folder/perjalanan_$number_file https://www.hrw.org/sites/default/files/styles/16x9_large/public/media_2022/12/202212asia_Indonesia_WR.jpg?h=adc9d34c&itok=Rd8EOF7l
    let number_file=number_file+1
else
    for((i=1; i<=$time; i=i+1))
    do
        wget -O ~/kumpulan_$number_folder/perjalanan_$number_file https://www.hrw.org/sites/default/files/styles/16x9_large/public/media_2022/12/202212asia_Indonesia_WR.jpg?h=adc9d34c&itok=Rd8EOF7l
        let number_file=number_file+1
    done
fi


next_nfile=$[$number_file]
sed -i "/#the variable that I want to be incremented$/s/=.*#/=$next_nfile;#/" ${0}
```

Penjelasan :
- Untuk menjalankan script selama 10 jam sekali maka ditulislah pada crontab dan menjalankan script kobeni_liburan.sh
- Untuk mencari nilai X dibuat dengan assign jam sekarang kedalam variabel time
- File didownload dengan format nama yaitu perjalanan_nomor, maka dibuatlah variabel number_file berubah sesuai jumlah file yang didownload dan berlanjut apabila script dijalankan lagi.
- File download disimpan didalam folder dengan format nama yaitu kumpulan_nomor, maka dibuatlah variabel number_folder berubah apabila script dijalankan lagi.
- Pertama pembuatan folder sesuai dengan format
- Kemudian mendownload dengan banyaknya file yang didownload sesuai dengan jam saat script dijalankan.
- File didownload sesuai dengan format nama dan disimpan kedalam folder yang sudah dibuat.

##### Soal b

Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

Penyelesaian :
```bash
number_zip=1;#the variable that I want to be increments
next_nzip=$[$number_zip+1]
sed -i "/#the variable that I want to be increments$/s/=.*#/=$next_nzip;#/" ${0}
zip -r devil_$number_zip.zip kumpul*
rm -r kumpulan*
```

Penjelasan :
- Script untuk zip sendiri harus dipisahkan dengan script yang sebelumnya
- Script zip ini dibuat di file zip.sh yang berada didalam satu file directory dengan folder yang dibuat sebelumnya
- Dengan increment variabel number_zip untuk penamaan folder zipnya
- Zip semua folder dengan nama depan kumpul dengan nama zip sesuai format
- Kemudian menghapus folder yang ada diluar zip untuk menghemat memori

##### Output
![output_prak_1](/uploads/e8e06f8c4b5a375c5d23f7e0dafea331/output_prak_1.jpeg)

![output_prak_1-2](/uploads/1c470bcbc84ebf4ed19d86fc4df335bd/output_prak_1-2.jpeg)

![output_prak_1-3](/uploads/7880777d119bb78a0a605c18bff6a27f/output_prak_1-3.jpeg)

## Soal 3

Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh. Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.

##### Soal a

louis.sh (sistem register) dengan ketentuan password sebagai berikut:
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username 
- Tidak boleh menggunakan kata chicken atau ernie

Penyelesaian :
```
#!/bin/bash
register(){
    echo "=========================================="
    echo "================ REGISTER ================"
    echo "=========================================="

    while true; do
        echo "Enter username: "
        read newUsername    
        if [ ${#newUsername} -eq 0 ]; then
            echo "Username should not be zero character"
            continue
        fi
        if grep -q "^${newUsername}:" /users/users.txt; then
            echo "$(date +"%Y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" | sudo tee -a /log.txt > /dev/null
            echo "Can not registered. Username already exists, please try different username."
            break
        fi
        required=0
        while [ $required -eq 0 ]; do
            echo "Enter password: "
            read -s newPassword
            if [ ${#newPassword} -ge 8 ]; then
                echo "$newPassword" | grep -q [A-Z]
                if test $? -eq 0; then
                    echo "$newPassword" | grep -q [a-z]
                    if test $? -eq 0; then
                        echo "$newPassword" | grep -q [0-9]
                        if test $? -eq 0; then
                            if [ "$newPassword" != $newUsername ]; then
                                if ! echo "$newPassword" | grep -q -i -E 'chicken|ernie'; then
                                    required=1
                                    echo "Registered successfully"
                                    echo "$(date +"%Y/%m/%d %H:%M:%S") REGISTER: INFO User $newUsername registered successfully" | sudo tee -a /log.txt > /dev/null
                                    echo "$newUsername:$newPassword" | sudo tee -a /users/users.txt > /dev/null
                                else
                                    echo "Password should not contain 'chicken' or 'ernie' word"
                                fi
                            else
                                echo "Password should not be the same as username"
                            fi
                        else
                            echo "Password should at least contain one number"
                        fi
                    else
                        echo "Password should at least contain one lowercase"
                    fi
                else
                    echo "Password should at least contain one uppercase"
                fi
            else
                echo "Password should at least contain 8 characters"
            fi
        done
        break
    done

    echo "=========================================="
    echo "=========================================="
    echo "=========================================="
}

if [ -d /users/ ]; then
    register
else
    sudo mkdir /users/ && sudo touch /users/users.txt
    register
fi
```

Penjelasan :
- Pertama-tama akan dilakukan pengecekan dir /users (if [ -d /users/ ]). Jika ada maka akan langsung menjalankan fungsi register. Jika tidak maka akan dibuat directorynya serta dibuat juga file user.txt terlebih dahulu dengan perintah sudo karena berada di dalam root directory.
- Selanjutnya dalam fungsi register, user akan terus diminta menginputkan username sampai username memenuhi kriteria (tidak sama dengan username yang sudah ada dan tidak boleh 0 karakter). Jika user menginput username yang telah dipakai, maka akan tercatat di dalam log.txt. Untuk mencocokan data username tersebut digunakan command grep (grep -q "^${newUsername}:" /users/users.txt).
- Setelah itu, user akan diminta terus menginputkan password yang harus sesuai dengan ketentuan yang diminta menggunakan nested loop dengan bantuan required flag. Sama halnya dengan username, password yang diinput akan di cek menggunakan command grep serta regex.
- Jika password sudah sesuai ketentuan, maka akan tercatat di dalam log.txt bahwa user telah berhasil terdaftar.
- Berikut contoh write error message ke dalam log.txt jika username telah dipakai:
(echo "$(date +"%Y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" | sudo tee -a /log.txt > /dev/null)

##### Soal b

retep.sh (sistem login) dengan pengecekan username dan password.

Penyelesaian :
```
#!/bin/bash
echo "=========================================="
echo "================= LOGIN =================="
echo "=========================================="

while true; do
    echo "Enter username: "
    read username
    if [ ${#username} -eq 0 ]; then
            echo "Username should not be zero character"
            continue
    fi
    if ! grep -q "^${username}:" /users/users.txt; then
        echo "Incorrect username or username does not exists"
        continue
    fi
    match=0
    while [ $match -eq 0 ]; do
        echo "Enter password: "
        read -s passwordInput
        password=$(grep "^${username}:" /users/users.txt | cut -d ':' -f 2)
        if [[ "$passwordInput" != "$password" ]]; then
            echo "$(date +"%Y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $username" | sudo tee -a /log.txt > /dev/null
            echo "Invalid password"
            break
        else
            match=1
            echo "$(date +"%Y/%m/%d %H:%M:%S") LOGIN: INFO User $username logged in" | sudo tee -a /log.txt > /dev/null
            echo "Logged in"
            break
        fi
    done
    break
done

echo "=========================================="
echo "=========================================="
echo "=========================================="
```

Penjelasan :
- Ketika shell script dijalankan, user akan terus diminta menginputkan username sampai username valid (sudah terdaftar dan tidak boleh 0 karakter). Pengecekan dilakukan pada directory  /users/users.txt menggunakan command grep.
- Ketika username valid, maka selanjutnya user diminta menginputkan password username tersebut. 
- Terdapat variabel password yang diambil menggunakan command grep dengan bantuan cut dengan ‘:’ sebagai delimiter dan -f untuk menetapkan field kedua sebagai password (field1=username:field2=password) untuk mencocokan password dari username yang telah diinputkan. Jika password yang dimasukan salah, maka akan ada pencatatan ke dalam log.txt dan user akan diminta memasukan password hingga benar. Jika password yang dimasukan benar, maka akan dicatat ke dalam log.txt bahwa user telah berhasil login.

Note:
- Ketika shell script dijalankan akan diminta password su terlebih dahulu, karena directory yang diminta berada di dalam root.
- log.txt juga disimpan kedalam directory root, karena tidak ada directory khusus yang diminta dan juga agar menjadi satu dir dengan folder users.

Kendala:
- Ketika ingin mencocokan username dan password pada sistem login dengan memastikan bahwa password yang dimasukkan sama dengan password dari usersname di /users/users.txt

Screenshots:
- louis.sh (sistem register)
![register](/uploads/d6396ccaa2ee4fc5fa724b48809e44be/register.png)

- retep.sh (sistem login)
![login](/uploads/794b3ba183130685195ad5058a6dcd9a/login.png)

- output (/users/users.txt &  log.txt)
![output](/uploads/666e801ace0fbd1df348d70d6bf78011/output.png)

## Soal4

##### Encrypt
```
#!/bin/bash

input_file="/var/log/syslog"

#check if the user have "Backup_Syslog" directory yet 
#if not, mkdir the directory first
if [ ! -d "$HOME/Documents/Backup_Syslog" ]; then
    mkdir -p "$HOME/Documents/Backup_Syslog"
fi

output_file="$(date +%H:%M\ %d:%m:%Y).txt"

hour=$(date +%k)

shift_amount=$((hour))

alphabet="abcdefghijklmnopqrstuvwxyz"
shifted_alphabet="${alphabet:${shift_amount}}${alphabet:0:${shift_amount}}"

ALPHABET="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
SHIFTED_ALPHABET="${ALPHABET:${shift_amount}}${ALPHABET:0:${shift_amount}}"

tr "${alphabet}${ALPHABET}" "${shifted_alphabet}${SHIFTED_ALPHABET}" < "$input_file" > "$output_file"

#reference
#https://askubuntu.com/questions/1097761/changing-individual-letter-position-with-bash
#https://gist.github.com/IQAndreas/030b8e91a8d9a407caa6
#https://www.geeksforgeeks.org/caesar-cipher-in-cryptography/
```

##### Decrypt
```
#!/bin/bash

# Set the input file name
input_file="$1"
# Get the hour from the input file name
hour=$(stat -c %y "$input_file" | cut -d' ' -f2 | cut -d':' -f1)
# Define the alphabet and shift amount
alphabet="abcdefghijklmnopqrstuvwxyz"
shift_amount=$((hour))

shifted_alphabet="${alphabet:${shift_amount}}${alphabet:0:${shift_amount}}"
ALPHABET="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

SHIFTED_ALPHABET="${ALPHABET:${shift_amount}}${ALPHABET:0:${shift_amount}}"

# Decrypt the input file and save the output
output_file="decrypted_${input_file}"
tr "${shifted_alphabet}${SHIFTED_ALPHABET}" "${alphabet}${ALPHABET}" < "$input_file" > "$output_file"
```

Penjelasan : 
Pada soal ini kita diminta untuk meng-enkripsi (Encrypt) isi dari file syslog yang ada pada OS, dimana syslog ini terdapat pada directory ```/var/log/syslog```. Kemudian setelah kita berhasil meng-enkripsi isi dari syslog, kita diminta untuk membuat script untuk meng-dekripsi (decrypt). Selain itu kita juga diminta untuk memberikan backup file syslog setiap 2 jam, backup ini dapat dilakukan dengan mudah dengan menjalankan script encrypt kita menggunakan crontab dengan timer ``` 0 */2 * * * path_ke_"log_encrypt.sh" ```. Perlu diperhatikan bahwa pada bagian m (minutes), karena apa bila pada bagian menit dibuat *, maka cron jobs akan menjalankan script setiap menit setiap jam kedua.

Untuk log_encrypt.sh, 
- kita perlu meng-shift alfabet dengan menggunakan nilai jam (hour) saat ini. untuk script ini kita mengambil nilai jam (hours) dengan menggunakan date ```+%k``` tidak lupa ```shift_amount=$((hour))``` yang memastikan bahwa shift_amount adalah angka bukan string. 

- Kemudian terdapat variabel alphabet dan ALPHABET yang berisikan alfabet dari a-z dan A-Z, yang kemudian akan di shift (geser) sebanyak nilai jam (hour) yang telah didapatkan sebelumnya dengan menggunakan parameter expansion 

```bash
alphabet="abcdefghijklmnopqrstuvwxyz"
shift_amount=$((hour))
shifted_alphabet="${alphabet:${shift_amount}}${alphabet:0:${shift_amount}}"
ALPHABET="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
SHIFTED_ALPHABET="${ALPHABET:${shift_amount}}${ALPHABET:0:${shift_amount}}"
```

- Langkah terakhir adalah untuk menggunakan tr command yang digunakan untuk menukar semua character tersebut dengan menggunakan 

```bash
    tr "${alphabet}${ALPHABET}" "${shifted_alphabet}${SHIFTED_ALPHABET}" < "$input_file" > $output_file"
```
- dapat dilihat tr diikuti dengan set 1 yaitu alfabet yang belum di shift kemudian alfabet yang telah di shift, lalu <$input_file artinya tr akan mengambil input dari alamat yang telah ditentukan dari input_file dan outputnya akan disimpan pada alamat yang ada pada output_file.

Untuk log_decrypt memiliki metode yang kurang lebih sama, perbedaanya hanyalah proses yang terbalik dari script enkripsi.
 
- Pertama kita memanggil terlebih dahulu script dekrip dengan ./log_decrypt [nama file yang ingin di dekrip],   
- lalu didalam script terdapat variabel input_file=$1 yang berarti input_file akan mengambil argumen 1 sebagai nilai/isinya. 
- kemudian terdapat deklarasi path untuk output_file yang berisikan path yang sama dengan file input tetapi dengan penamaan yang ditambahkan ‘decrypted_’ didepannya. 
- Kemudian karena kita menggunakan jam pada masa dibuat file backup, maka kita tinggal menggunakan jam yang ada pada nama file yang dienkripsi, hal ini dapat kita lakukan dengan menggunakan hour=$(stat -c %y "$input_file" | cut -d' ' -f2 | cut -d':' -f1), command ini akan mengambil waktu dari file yang dienkripsi tersebut dibuat\
    
- kemudian proses shift alfabet yang dilakukan terbalik, dimana set 1 dan set 2 dibalik

```bash
    tr "${shifted_alphabet}${SHIFTED_ALPHABET}" "${alphabet}${ALPHABET}" < "$input_file" > "$output_file"
```

- dapat dilihat bahwa SET 1 nya berisikan shifted alfabet dan SET 2 nya berisikan alfabet normal, hal ini dilakukan untuk me-reverse enkripsi yang dilakukan sebelumnya. 

kendala keseluruhan soal: 
- ketika ingin menggunakan for loop untuk mengganti semua char di text, ternyata membutuhkan waktu yang terlalu lama, sehinggal harus mencari metode baru menggunakan "tr"
- pada awalnya dekripsi ingin dilakukan dengan mengambil hours dari judul txt, tetapi lebih mudah dan terjamin menggunakan stat (karena judul txt adalah string)

screenshot :

running code:
![image](/uploads/d8d6d62210ac3e9773920d39abf8bda4/image.png)
encrypted file:
![image](/uploads/0e7a6c6d480b7827301d23e56c5daccc/image.png)
decrypted file:
![image](/uploads/7f3d4622f9197635012fd2a32d360d09/image.png) 