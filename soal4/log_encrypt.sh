#!/bin/bash

input_file="/var/log/syslog"

#check if the user have "Backup_Syslog" directory yet 
#if not, mkdir the directory first
if [ ! -d "$HOME/Documents/Backup_Syslog" ]; then
    mkdir -p "$HOME/Documents/Backup_Syslog"
fi

output_file="$(date +%H:%M\ %d:%m:%Y).txt"

hour=$(date +%k)

shift_amount=$((hour))

alphabet="abcdefghijklmnopqrstuvwxyz"
shifted_alphabet="${alphabet:${shift_amount}}${alphabet:0:${shift_amount}}"

ALPHABET="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
SHIFTED_ALPHABET="${ALPHABET:${shift_amount}}${ALPHABET:0:${shift_amount}}"

tr "${alphabet}${ALPHABET}" "${shifted_alphabet}${SHIFTED_ALPHABET}" < "$input_file" > "$output_file"

#reference
#https://askubuntu.com/questions/1097761/changing-individual-letter-position-with-bash
#https://gist.github.com/IQAndreas/030b8e91a8d9a407caa6
#https://www.geeksforgeeks.org/caesar-cipher-in-cryptography/
#CRON JOBS FOR EVERY 2 HOURS	
#0 */2 * * * $HOME/Documents/Praktikum1Sisop/log_encrypt.sh

