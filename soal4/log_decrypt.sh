#!/bin/bash

# Set the input file name
input_file="$1"
# Get the hour from the input file name
hour=$(stat -c %y "$input_file" | cut -d' ' -f2 | cut -d':' -f1)
# Define the alphabet and shift amount
alphabet="abcdefghijklmnopqrstuvwxyz"
shift_amount=$((hour))

shifted_alphabet="${alphabet:${shift_amount}}${alphabet:0:${shift_amount}}"
ALPHABET="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

SHIFTED_ALPHABET="${ALPHABET:${shift_amount}}${ALPHABET:0:${shift_amount}}"

# Decrypt the input file and save the output
output_file="decrypted_${input_file}"
tr "${shifted_alphabet}${SHIFTED_ALPHABET}" "${alphabet}${ALPHABET}" < "$input_file" > "$output_file"
