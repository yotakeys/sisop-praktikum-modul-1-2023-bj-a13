#!/bin/bash
echo "=========================================="
echo "================= LOGIN =================="
echo "=========================================="

while true; do
    echo "Enter username: "
    read username
    if [ ${#username} -eq 0 ]; then
            echo "Username should not be zero character"
            continue
    fi
    if ! grep -q "^${username}:" /users/users.txt; then
        echo "Incorrect username or username does not exists"
        continue
    fi
    match=0
    while [ $match -eq 0 ]; do
        echo "Enter password: "
        read -s passwordInput
        password=$(grep "^${username}:" /users/users.txt | cut -d ':' -f 2)
        if [[ "$passwordInput" != "$password" ]]; then
            echo "$(date +"%Y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $username" | sudo tee -a /log.txt > /dev/null
            echo "Invalid password"
            break
        else
            match=1
            echo "$(date +"%Y/%m/%d %H:%M:%S") LOGIN: INFO User $username logged in" | sudo tee -a /log.txt > /dev/null
            echo "Logged in"
            break
        fi
    done
    break
done

echo "=========================================="
echo "=========================================="
echo "=========================================="
