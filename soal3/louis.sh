#!/bin/bash
register(){
    echo "=========================================="
    echo "================ REGISTER ================"
    echo "=========================================="

    while true; do
        echo "Enter username: "
        read newUsername    
        if [ ${#newUsername} -eq 0 ]; then
            echo "Username should not be zero character"
            continue
        fi
        if grep -q "^${newUsername}:" /users/users.txt; then
            echo "$(date +"%Y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" | sudo tee -a /log.txt > /dev/null
            echo "Can not registered. Username already exists, please try different username."
            break
        fi
        required=0
        while [ $required -eq 0 ]; do
            echo "Enter password: "
            read -s newPassword
            if [ ${#newPassword} -ge 8 ]; then
                echo "$newPassword" | grep -q [A-Z]
                if test $? -eq 0; then
                    echo "$newPassword" | grep -q [a-z]
                    if test $? -eq 0; then
                        echo "$newPassword" | grep -q [0-9]
                        if test $? -eq 0; then
                            if [ "$newPassword" != $newUsername ]; then
                                if ! echo "$newPassword" | grep -q -i -E 'chicken|ernie'; then
                                    required=1
                                    echo "Registered successfully"
                                    echo "$(date +"%Y/%m/%d %H:%M:%S") REGISTER: INFO User $newUsername registered successfully" | sudo tee -a /log.txt > /dev/null
                                    echo "$newUsername:$newPassword" | sudo tee -a /users/users.txt > /dev/null
                                else
                                    echo "Password should not contain 'chicken' or 'ernie' word"
                                fi
                            else
                                echo "Password should not be the same as username"
                            fi
                        else
                            echo "Password should at least contain one number"
                        fi
                    else
                        echo "Password should at least contain one lowercase"
                    fi
                else
                    echo "Password should at least contain one uppercase"
                fi
            else
                echo "Password should at least contain 8 characters"
            fi
        done
        break
    done

    echo "=========================================="
    echo "=========================================="
    echo "=========================================="
}

if [ -d /users/ ]; then
    register
else
    sudo mkdir /users/ && sudo touch /users/users.txt
    register
fi
